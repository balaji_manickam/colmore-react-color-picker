# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


In the project directory, you can run:

## Installation

Execute the npm command initially `npm install`

## Running

Execute the npm command to start application locally `npm start`

Runs the app in the development mode. Open http://localhost:3000 to view it in the browser.


The page will reload if you make edits.\
You will also see any lint errors in the console.
