import { useState } from "react";
import { ColorResult, RGBColor, SketchPicker } from "react-color";
import "./App.css";

function App() {
  const [color, setColor] = useState<RGBColor>({
    r: 241,
    g: 112,
    b: 19,
    a: 0.3,
  });

  const handleChangeColor = (color: ColorResult) => {
    setColor(color.rgb);
  };

  return (
    <div
      className="App"
      style={{
        backgroundColor: `rgba(${color.r}, ${color.g}, ${color.b}, ${color.a})`,
      }}
    >
      <SketchPicker color={color} onChange={handleChangeColor} />
    </div>
  );
}

export default App;
